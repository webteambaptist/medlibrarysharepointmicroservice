﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using MedLibrarySharepointMicroservice.Models;
using Newtonsoft.Json;
using NLog;
using System.Configuration;
using Microsoft.SharePoint.Client;

namespace MedLibrarySharepointMicroservice.Controllers
{
    [RoutePrefix("api/librarysearch")]
    public class LibrarySearchController : ApiController
    {
        private static readonly Logger Logger = NLog.LogManager.GetCurrentClassLogger();
        private static readonly string MedicalLibrary = ConfigurationManager.AppSettings["MedicalLibrary"];
        // GET: api/LibrarySearch
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        [HttpPost]
        [Route("AddToLibrary")]
        public IHttpActionResult AddToLibrary()
        {
            var config = new NLog.Config.LoggingConfiguration();
            var logFile = new NLog.Targets.FileTarget("logfile") { FileName = $"Logs\\LibrarySearch{DateTime.Today:MM-dd-yy}.log" };
            config.AddRule(LogLevel.Info, LogLevel.Fatal, logFile);
            NLog.LogManager.Configuration = config;

            try
            {
                var headers = Request.Headers;
                if (headers.Contains("LibraryRequest"))
                {
                    var request = headers.GetValues("LibraryRequest").First();
                    var searchRequest = JsonConvert.DeserializeObject<LibrarySearch>(request);

                    try
                    {
                        using (var context = new ClientContext(MedicalLibrary))
                        {
                            context.Credentials = new NetworkCredential("wedadmsvc", "w3d4DM$vC", "bh");
                            var libraryList = context.Web.Lists.GetByTitle("Library Search Request");
                            var itemCreation = new ListItemCreationInformation();
                            var item = libraryList.AddItem(itemCreation);
                            item["Title"] = searchRequest.Name;
                            item["eMail"] = searchRequest.Email;
                            item["Current_x0020_Date"] = searchRequest.CurrentDate;
                            item["Date_x0020_Needed_x0020_By"] = searchRequest.DateNeededBy;
                            item["Rush"] = searchRequest.Rush;
                            item["Profession"] = searchRequest.Profession;
                            item["Department"] = searchRequest.Department;
                            item["Location"] = searchRequest.Location;
                            item["Search_x0020_Request"] = searchRequest.SearchRequest;
                            item["Search_x0020_Qualifications"] = searchRequest.SearchQualifications;
                            item["Ages"] = searchRequest.Ages;
                            item["Years_x0020_to_x0020_be_x0020_co"] = searchRequest.YearsToBeCovered;
                            item["Kind_x0020_of_x0020_search_x0020"] = searchRequest.KindOfSearch;
                            item["Purpose_x0020_of_x0020_the_x0020"] = searchRequest.PurposeOfSearch;
                            item["Delivery_x0020_Type"] = searchRequest.DeliveryType;
                            item["Phone"] = searchRequest.Phone;

                            item.Update();
                            context.ExecuteQuery();
                        }

                    }
                    catch (Exception e)
                    {
                        Logger.Error("Exception writing to Sharepoint :: AddToLibrary " + e.Message);
                        return BadRequest(e.Message);
                    }

                }

            }
            catch (Exception e)
            {
                Logger.Error("Exception occurred :: AddToLibrary :: " + e.Message);
                return BadRequest(e.Message);
            }

            return Ok();
        }

        // GET: api/LibrarySearch/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/LibrarySearch
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/LibrarySearch/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/LibrarySearch/5
        public void Delete(int id)
        {
        }
    }
}
