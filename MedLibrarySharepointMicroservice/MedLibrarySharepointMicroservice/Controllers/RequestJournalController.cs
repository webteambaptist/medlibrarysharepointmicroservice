﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using MedLibrarySharepointMicroservice.Models;
using Microsoft.SharePoint.Client;
using Newtonsoft.Json;
using NLog;

namespace MedLibrarySharepointMicroservice.Controllers
{
    [RoutePrefix("api/journal")]
    public class RequestJournalController : ApiController
    {
        private static readonly Logger Logger = NLog.LogManager.GetCurrentClassLogger();
        private static readonly string MedicalLibrary = ConfigurationManager.AppSettings["MedicalLibrary"];
        [HttpPost]
        [Route("AddToJournal")]
        public IHttpActionResult AddToLibrary()
        {
            var config = new NLog.Config.LoggingConfiguration();
            var logFile = new NLog.Targets.FileTarget("logfile") { FileName = $"Logs\\JournalRequest{DateTime.Today:MM-dd-yy}.log" };
            config.AddRule(LogLevel.Info, LogLevel.Fatal, logFile);
            NLog.LogManager.Configuration = config;

            try
            {
                var headers = Request.Headers;
                if (headers.Contains("JournalRequest"))
                {
                    var request = headers.GetValues("JournalRequest").First();
                    var journalRequest = JsonConvert.DeserializeObject<JournalRequest>(request);

                    try
                    {
                        using (var context = new ClientContext(MedicalLibrary))
                        {
                            context.Credentials = new NetworkCredential("wedadmsvc", "w3d4DM$vC", "bh");
                            var libraryList = context.Web.Lists.GetByTitle("Request Journal Articles");
                            var itemCreation = new ListItemCreationInformation();
                            var item = libraryList.AddItem(itemCreation);
                            item["Title"] = journalRequest.Name;
                            item["eMail"] = journalRequest.Email;
                            item["Current_x0020_Date"] = journalRequest.CurrentDate;
                            item["Date_x0020_Needed_x0020_By"] = journalRequest.DateNeededBy;
                            item["Rush"] = journalRequest.Rush;
                            item["Profession"] = journalRequest.Profession;
                            item["Department"] = journalRequest.Department;
                            item["Location"] = journalRequest.Location;
                            item["Journal_x0020_Article_x0020_Requ"] = journalRequest.JournalArticleRequest;
                            item["Book_x0020_Request"] = journalRequest.BookRequest;
                            item["Delivery_x0020_Type"] = journalRequest.DeliveryType;
                            item["Phone_x0020__x0023_"] = journalRequest.Phone;
                            item["First_x0020_Publication_x0020_Su"] = journalRequest.FirstPublicationSupplied;
                            item["_x0031__x0020__x002d__x0020_Publ"] = journalRequest.FirstPublicationYearDate;
                            item["_x0032__x0020__x002d__x0020_Publ"] = journalRequest.SecondPublicationSupplied;
                            item["_x0032__x0020__x002d__x0020_Publ0"] = journalRequest.SecondPublicationYearDate;
                            item["_x0033__x0020__x002d__x0020_Publ"] = journalRequest.ThirdPublicationSupplied;
                            item["_x0033__x0020__x002d__x0020_Publ0"] = journalRequest.ThirdPublicationYearDate;
                            item["_x0034__x0020__x002d__x0020_Publ"] = journalRequest.ForthPublicationSupplied;
                            item["_x0034__x0020__x002d__x0020_Publ0"] = journalRequest.ForthPublicationYearDate;
                            item["_x0035__x0020__x002d__x0020_Publ"] = journalRequest.FifthPublicationSupplied;
                            item["_x0035__x0020__x002d__x0020_Publ0"] = journalRequest.FifthPublicationYearDate;
                            item.Update();
                            context.ExecuteQuery();
                        }

                    }
                    catch (Exception e)
                    {
                        Logger.Error("Exception writing to Sharepoint :: AddToLibrary " + e.Message);
                        return BadRequest(e.Message);
                    }

                }

            }
            catch (Exception e)
            {
                Logger.Error("Exception occurred :: AddToLibrary :: " + e.Message);
                return BadRequest(e.Message);
            }

            return Ok();
        }
        // GET: api/RequestJournal
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/RequestJournal/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/RequestJournal
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/RequestJournal/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/RequestJournal/5
        public void Delete(int id)
        {
        }
    }
}
