﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MedLibrarySharepointMicroservice.Models
{
    public class JournalRequest
    {
        public string Name { get;set; }
        public string Email { get;set; }
        public DateTime CurrentDate { get; set; }
        public DateTime DateNeededBy { get;set; }
        public bool Rush { get; set; }
        public string Profession { get; set; }
        public string Department { get; set; }
        public string Location { get; set; }
        public string JournalArticleRequest { get; set; }
        public string BookRequest { get; set; }
        public string DeliveryType { get; set; }
        public string Phone { get; set; }
        public string FirstPublicationSupplied { get; set; }
        public string FirstPublicationYearDate { get; set; }
        public string SecondPublicationSupplied { get; set; }
        public string SecondPublicationYearDate { get; set; }
        public string ThirdPublicationSupplied { get; set; }
        public string ThirdPublicationYearDate { get; set; }
        public string ForthPublicationSupplied { get; set; }
        public string ForthPublicationYearDate { get; set; }
        public string FifthPublicationSupplied { get; set; }
        public string FifthPublicationYearDate { get; set; }
    }
}