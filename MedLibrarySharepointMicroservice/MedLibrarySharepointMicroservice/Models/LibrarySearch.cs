﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MedLibrarySharepointMicroservice.Models
{
    public class LibrarySearch
    {
        public string Name { get;set; }
        public string Email { get;set; }
        public DateTime CurrentDate { get; set; }
        public DateTime DateNeededBy { get;set; }
        public bool Rush { get; set; }
        public string Profession { get; set; }
        public string Department { get; set; }
        public string Location { get; set; }
        public string SearchRequest { get; set; }
        public string SearchQualifications { get; set; }
        public string Ages { get; set; }
        public string YearsToBeCovered { get; set; }
        public string KindOfSearch { get; set; }
        public string PurposeOfSearch { get; set; }
        public string DeliveryType { get; set; }
        public string Phone { get; set; }
    }
}